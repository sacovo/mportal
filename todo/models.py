from django.db import models

from django.contrib.auth.models import User
from mitglieder.models import Member
# Create your models here.

class Task(models.Model):
    title = models.CharField(max_length=250)
    description = models.TextField()
    users = models.ManyToManyField(User)
    due_date = models.DateTimeField()

    def __str__(self):
        return self.title

class TaskItem(models.Model):
    task = models.ForeignKey(Task, models.CASCADE)
    member = models.ForeignKey(Member, models.CASCADE)
    responsible = models.ForeignKey(User, models.CASCADE)
    done = models.BooleanField(default=False)

    def __str__(self):
        return self.task.title

