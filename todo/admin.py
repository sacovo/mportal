from django.contrib import admin
from todo.models import Task, TaskItem
# Register your models here.

class TaskItemInline(admin.TabularInline):
    model = TaskItem
    extra = 3
    fields = ['member', 'responsible', 'done']
    raw_id_fields = ('member',)

@admin.register(Task)
class TaskAdmin(admin.ModelAdmin):
    inlines = [TaskItemInline]
    fields = ['title', 'description', 'users', 'due_date']
    filter_horizontal = ['users']
    list_display = ('title', 'due_date')
    date_hierarchy = 'due_date'
    search_fields = ('title', 'description')

@admin.register(TaskItem)
class TaskItemAdmin(admin.ModelAdmin):
    list_display = ('task', 'member', 'responsible', 'done')
    list_editable = ('done',)
    raw_id_fields = ('member',)
    list_filter = ('done', 'task', 'responsible')
    date_hierarchy = 'task__due_date'
    search_fields = ('member__first_name', 'task__title', 'member__last_name', 'task__description',
            'member__email')
