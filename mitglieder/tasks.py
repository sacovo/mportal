# coding=utf-8
""" This file is part of Sektionsportal.
    Sektionsportal is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Sektionsportal is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Sektionsportal.  If not, see <http://www.gnu.org/licenses/>.
"""
from __future__ import absolute_import
import datetime
from celery import shared_task
from twilio.rest import Client
from webob.multidict import MultiDict

from premailer import transform

from django.core import serializers, mail
from django.core.mail import EmailMultiAlternatives as EmailMessage
from django.template import Template, Context, Engine, engines
from django.utils.html import mark_safe

from django.conf import settings

import requests
import pandas as pd
import bleach
import json
import os
import html2text
from mitglieder.models import Member, SMSSender, MailGunUser,\
    MailGunMessage, Group, MailTemplate, SMS, WebSMSAccount

@shared_task
def send_wa_websms(content, pk, group, attachment=None):
    sender = WebSMSAccount.objects.get(group__pk=group)
    token = sender.token
    url = 'https://api.websms.com/rest/converged/whatsapp'
    headers = {'authorization': 'Bearer ' + token, 'content-type': 'application/json', 'accept': 'application/json'}

    recipient = Member.objects.get(pk=pk)
    number = recipient.get_mobile

    if number:
        t = engines['django'].from_string(content)
        context = {'contact': recipient}
        cont = t.render(context=context)

        data = {'messageContent': cont, 'recipientAddressList': ['41' + number[1:]]}
        if attachment:
            data.update({'attachmentId': attachment})

        r = requests.post(url, data=json.dumps(data), headers=headers)
        print(r.json())

@shared_task
def send_sms(content, pk, group, whatsapp=False):
    """Sends a sms to all recipients"""
    sender = SMSSender.objects.get(group__pk=group)
    auth = sender.auth
    sid = sender.sid
    user = sender.username
    client = Client(user, auth, user)
    recipient = Member.objects.get(pk=pk)
    number = recipient.get_mobile
    if number:
        number = "+41" + number[1:]
        if whatsapp:
            number = "whatsapp:+41" + number[1:]
        t = engines['django'].from_string(content)
        context = {'contact': recipient}
        cont = t.render(context=context)
        from_ = sender.number
        if whatsapp:
            from_ = 'whatsapp:' + sender.number
        message = client.messages.create(body=cont, to=number,
                                         from_=from_,
                                         messaging_service_sid=sender.sid)
        SMS.objects.create(
                content=cont,
                member=recipient,
                incoming=False,
                twilio_account=sender,
                twilio_sid=message.sid,
                delivered=False
                )
        return False

    return True


def render_html(message, etype, img_url, sender, template):
    """Docstring"""
    engine = Engine.get_default()
    if template:
        s = MailTemplate.objects.get(pk=template).template.read().decode()
    else:
        s = '{% autoescape off%}{{message}}{% endautoescape %}'
    template = engine.from_string(s)
    html = template.render(Context({
        'message': bleach.clean(message, tags=bleach.sanitizer.ALLOWED_TAGS + ['p'], strip=True),
        'etype': etype,
        'img_url': img_url,
        'from': sender,
    }))
    return transform(html)



@shared_task
def send_mail(content, recipient, attachments, **kwargs):
    """Docstring"""
    for key in kwargs:
        if type(kwargs[key]) == str:
            kwargs[key] = mark_safe(kwargs[key])
    recipient = Member.objects.get(pk=recipient)
    mail_ = EmailMessage(content['subject'].format(contact=recipient))
    message = content['message']
    t = engines['django'].from_string(message)
    context = {'contact': recipient}
    context.update(kwargs)
    message = t.render(context=context)
    mail_.body = html2text.html2text(message)
    message = render_html(message, content['etype'], content['img_url'],
                          content['sender'], content['template'])
    mail_.attach_alternative(message, 'text/html')
    mail_.from_email = '{name} <{email}>'.format(
        email=content['sender'], name=content['name'])
    mail_.to = [recipient.email]
    for att in attachments:
        mail_.attach(att['name'], open(att['path'], 'rb').read())

    connection = mail.get_connection()
    connection.send_messages([mail_])
    return "Success!"


@shared_task
def check_birthday(info_mail, body_text, subject):
    """TODO"""
    today = datetime.date.today()
    future = today + datetime.timedelta(days=2)
    members = Member.objects.filter(
        birthday__day=future.day, birthday__month=future.month)
    if members.count() == 0:
        return 'No upcomming birthdays!'
    message = EmailMessage(subject=subject)
    template = Template(body_text)
    context = Context({'member_list': members})
    message.body = template.render(context)
    message.to = [info_mail]
    message.from_email = 'JUSO Aargau <info@juso-aargau.ch>'
    message.send()
    return '{x} birthdays sent'.format(x=members.count())


@shared_task
def send_mail_mg(content, recipient, attachments, group, mg_pk, **kwargs):
    for key in kwargs:
        if type(kwargs[key]) == str:
            kwargs[key] = mark_safe(kwargs[key])
    """Docstring"""
    recipient = Member.objects.get(pk=recipient)
    mdict = MultiDict()
    mdict.add('from', '{name} <{email}>'.format(
        email=content['sender'], name=content['name']))
    mdict.add('to', recipient.email)
    mdict.add('subject', content['subject'].format(contact=recipient))
    mdict.add('o:tag', content['tag'])
    mdict.add('v:message-id', mg_pk)
    message = content['message']
    t = engines['django'].from_string(message)
    context = {'contact': recipient}
    context.update(kwargs)
    message = t.render(context=context)

    mdict.add('text', html2text.html2text(message))
    message = render_html(message, content['tag'], content['img_url'],
                          content['sender'], content['template'])
    mdict.add('html', message)

    mg_user = MailGunUser.objects.get(group__pk=group)
    mail_ = mdict
    url = "https://api.mailgun.net/v3/{domain}/messages".format(
        domain=mg_user.domain)
    auth = ('api', mg_user.api_key)
    files = []
    for att in attachments:
        files.append(('attachment', open(att['path'], 'rb')))

    r = requests.post(url, auth=auth, data=mail_, files=files)

    return r.status_code

@shared_task
def check_mail(group, email, recipients):
    recipients = Member.objects.filter(npk__in=recipients)
    group = Group.objects.get(pk=group)
    pub_key = MailGunUser.objects.filter(group=group).exclude(pub_key='')[0].pub_key
    mail_list = recipients.values('email')

    url = "https://api.mailgun.net/v3/address/private/validate"
    if pub_key.startswith('pub'):
        url = "https://api.mailgun.net/v3/address/validate"

    results = []
    valid_count = 0
    invalid_count = 0
    disposable_count = 0
    for recipient in recipients:
        r = requests.get(url, auth=('api', pub_key), params={
            'address': recipient.email,
            'mailbox_verification': True
        })
        result = json.loads(r.text)
        result.update({
            'first_name': recipient.first_name,
            'last_name': recipient.last_name,
            'om_number': recipient.om_number
        })
        if result['is_valid']:
            valid_count += 1
        else:
            invalid_count += 1
        if result['is_disposable_address']:
            disposable_count += 1
        results.append(result)
    df = pd.DataFrame(results).set_index('address')
    today = datetime.datetime.now()
    name = '{today:%Y-%m-%d-%H%M%s}-{mail}.xlsx'.format(today=today, mail=email)
    path = os.path.join(settings.MEDIA_ROOT, 'reports', name)
    df.to_excel(path)
    msg = """Dein Report ist fertig!\n
Du kannst ihn hier herunterladen: https://{url}{media}reports/{name}\n
Gültige Adressen: {valid}
Ungültige Adressen: {invalid}
Wegwerfadressen: {disposable}"""
    msg = msg.format(url=settings.SITE_DOMAIN, media=settings.MEDIA_URL,
            name=name, valid=valid_count, invalid=invalid_count, disposable=disposable_count)

    email_ = EmailMessage(
            'Report - Sektionsportal',
            msg,
            settings.DEFAULT_FROM_EMAIL,
            [email,]
    )

    connection = mail.get_connection()
    connection.send_messages([email_])
