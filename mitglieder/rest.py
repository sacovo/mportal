from django.conf.urls import url, include
from django.contrib.auth.models import User, Group
from rest_framework import routers, serializers, viewsets
from mitglieder.models import Member, ContactList, Membership,\
        Skill, Occupation, Membership, LetterTemplate, TemplateField,\
        PortalForm, FormField, MemberField, PortalFormAnswer, FormFieldAnswer,\
        MailGunUser, MailTemplate, MailGunMessage, MailGunEvent, SMSSender, SMS
from django.db.models import Q
from rest_framework.decorators import action
from django_filters import rest_framework as filters
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import filters
from django.core import serializers as django_serializers

from mitglieder.tasks import send_sms as send_sms_celery,\
        send_mail as send_mail_celery, send_mail_mg as send_mail_mg_celery

class MailgunEventSerializer(serializers.HyperlinkedModelSerializer):
    recipient = serializers.PrimaryKeyRelatedField(read_only=True)

    class Meta:
        model = MailGunEvent
        fields = ('pk', 'url', 'recipient', 'country',
                'device_type', 'client_type', 'client_os',
                'tag', 'timestamp', 'event')

class MailgunMessageSerializer(serializers.HyperlinkedModelSerializer):
    group = serializers.PrimaryKeyRelatedField(read_only=True)
    recipients = serializers.PrimaryKeyRelatedField(read_only=True, many=True)
    mailgunevent_set = MailgunEventSerializer(read_only=True, many=True)

    class Meta:
        model = MailGunMessage
        fields = ('pk', 'url', 'group', 'created', 'subject',
                'sender', 'content', 'recipients', 'tag', 'open_points',
                'click_points', 'remove_points', 'mailgunevent_set')

class MailgunSerializer(serializers.HyperlinkedModelSerializer):
    group = serializers.PrimaryKeyRelatedField(read_only=True)
    class Meta:
        model = MailGunUser
        fields = ('url', 'group', 'domain', 'api_key', 'pk')

class MailgunViewSet(viewsets.ModelViewSet):
    queryset = MailGunUser.objects.all()
    serializer_class = MailgunSerializer

    def get_queryset(self):
        return super().get_queryset().filter(group__in=self.request.user.groups.all()).distinct()

class TwilioSerializer(serializers.HyperlinkedModelSerializer):
    group = serializers.PrimaryKeyRelatedField(read_only=True)
    class Meta:
        model = SMSSender
        fields = ('url', 'group', 'number', 'sid', 'username', 'auth', 'pk')

class TwilioViewSet(viewsets.ModelViewSet):
    queryset = SMSSender.objects.all()
    serializer_class = TwilioSerializer

    def get_queryset(self):
        return super().get_queryset().filter(group__in=self.request.user.groups.all()).distinct()

class FormFieldSerializer(serializers.HyperlinkedModelSerializer):
    form = serializers.PrimaryKeyRelatedField(queryset=PortalForm.objects.all())
    class Meta:
        model = FormField
        fields = ('var_name', 'input_type', 'form', 'ordering', 'required', 'initial',
                'choices', 'widget', 'pk', 'label')

class AnswerSerializer(serializers.ModelSerializer):
    form_field = serializers.PrimaryKeyRelatedField(read_only=True)
    class Meta:
        model = FormFieldAnswer
        fields = ('value', 'form_field')

class MemberFieldSerializer(serializers.ModelSerializer):
    class Meta:
        model = MemberField
        fields = ('var_name', 'label', 'ordering')

class PortalFormAnswerSerializer(serializers.ModelSerializer):
    member = serializers.PrimaryKeyRelatedField(read_only=True)
    answers = AnswerSerializer(many=True, read_only=True)
    memberfield_set = MemberFieldSerializer(read_only=True, many=True)
    class Meta:
        model = PortalFormAnswer
        fields = ('pk', 'url', 'member', 'confirmed', 'answers', 'memberfield_set')

class PortalFormSerializer(serializers.HyperlinkedModelSerializer):
    users = serializers.PrimaryKeyRelatedField(read_only=True, many=True)
    destination_group = serializers.PrimaryKeyRelatedField(read_only=True)
    allowed_groups = serializers.PrimaryKeyRelatedField(queryset=Group.objects.all(), many=True)
    fields = FormFieldSerializer(read_only=True, many=True)
    portalformanswer_set = PortalFormAnswerSerializer(read_only=True, many=True)
    class Meta:
        model = PortalForm
        fields = ('pk', 'url','name', 'slug', 'description', 'allowed_groups',
                'color', 'destination_group', 'email_field_name',
                'confirmation_text', 'contact_name', 'contact_mail',
                'users', 'editable', 'portalformanswer_set', 'fields')

class MailgunViewSet(viewsets.ModelViewSet):
    queryset = MailGunUser.objects.all()
    serializer_class = MailgunSerializer

    def get_queryset(self):
        queryset = super().get_queryset()
        groups = self.request.user.groups.all()
        return queryset.filter(group__in=groups).distinct()

class MailgunMessageViewSet(viewsets.ModelViewSet):
    queryset = MailGunMessage.objects.all()
    serializer_class = MailgunMessageSerializer

    def get_queryset(self):
        queryset = super().get_queryset()
        groups = self.request.user.groups.all()
        return queryset.filter(group__in=groups).distinct()

class PortalFormViewSet(viewsets.ModelViewSet):
    queryset = PortalForm.objects.all()
    serializer_class = PortalFormSerializer

    def get_queryset(self):
        return super().get_queryset().filter(users=self.request.user)

class TemplateFieldSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = TemplateField
        fields = ('pk','var_name', 'input_type', 'label', 'ordering', 'argument_string')

class MailTemplateSerializer(serializers.ModelSerializer):
    class Meta:
        model = MailTemplate
        fields = ('pk', 'name')

class MailTemplateViewSet(viewsets.ModelViewSet):
    queryset = MailTemplate.objects.all()
    serializer_class = MailTemplateSerializer

class PortalFormSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = PortalForm

class LetterTemplateSerializer(serializers.HyperlinkedModelSerializer):
    fields = TemplateFieldSerializer(read_only=True, many=True)
    class Meta:
        model = LetterTemplate
        fields = ('ulr', 'pk', 'name', 'fields')

class LetterTemplateViewSet(viewsets.ModelViewSet):
    queryset = LetterTemplate.objects.all()
    serializer_class = LetterTemplateSerializer

class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ('url','pk', 'username', 'email', 'is_staff')

# ViewSets define the view behavior.
class UserViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer

    def get_queryset(self):
        return User.objects.filter(pk=self.request.user.pk)

class GroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Group
        fields = ('url', 'name',)

class GroupViewSet(viewsets.ModelViewSet):
    queryset = Group.objects.all()
    serializer_class = GroupSerializer

    def get_queryset(self):
        return self.request.user.groups.all()

class ContactListSerializer(serializers.HyperlinkedModelSerializer):
    member_set = serializers.PrimaryKeyRelatedField(read_only=True, many=True)
    class Meta:
        model = ContactList
        fields = ('url','pk', "name", "users", "email", "member_set")

class ContactListViewSet(viewsets.ModelViewSet):
    queryset = ContactList.objects.all()
    serializer_class = ContactListSerializer

    def get_queryset(self):
        return self.request.user.contactlist_set.all()

class MembershipSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Membership
        fields = ('url', 'name', "description", 'pk')

class MembershipViewSet(viewsets.ModelViewSet):
    queryset = Membership.objects.all()
    serializer_class = MembershipSerializer

class SkillSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Skill
        fields = ('url', 'name', "description", 'pk')

class SkillViewSet(viewsets.ModelViewSet):
    queryset = Skill.objects.all()
    serializer_class = SkillSerializer

class OccupationSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Occupation
        fields = ('url', 'name', "description", 'pk')

class OccupationViewSet(viewsets.ModelViewSet):
    queryset = Occupation.objects.all()
    serializer_class = OccupationSerializer

class MemberSerializer(serializers.HyperlinkedModelSerializer):
    position = serializers.PrimaryKeyRelatedField(many=True, queryset=Group.objects.all())
    memberships = serializers.PrimaryKeyRelatedField(many=True, queryset=Membership.objects.all())
    occupations = serializers.PrimaryKeyRelatedField(many=True, queryset=Occupation.objects.all())
    skills = serializers.PrimaryKeyRelatedField(many=True, queryset=Skill.objects.all())
    contact_list = serializers.PrimaryKeyRelatedField(queryset=ContactList.objects.all())

    class Meta:
        model = Member
        fields = ("url", "first_name", "last_name", 'email',
                'phone_mobi', 'phone_busi', 'phone_priv',
                'street', 'plz', 'city', 'gender', 'birthday',
                'status', 'activity_points', 'mail_activity',
                'language', 'allow_contact', 'position', 'contact_list',
                'occupations', 'skills', 'memberships', 'pk')

class MailgunMailSerializer(serializers.Serializer):
    group = serializers.PrimaryKeyRelatedField(queryset=Group.objects.all())
    name = serializers.CharField()
    sender = serializers.EmailField()
    tag = serializers.CharField()
    subject = serializers.CharField()
    img_url = serializers.URLField()
    remove_points = serializers.IntegerField()
    open_points = serializers.IntegerField()
    click_points = serializers.IntegerField()
    message = serializers.CharField()
    template = serializers.PrimaryKeyRelatedField(queryset=MailTemplate.objects.all())
    eta = serializers.DateTimeField()
    recipients = serializers.PrimaryKeyRelatedField(queryset=Member.objects.all(), many=True)

class SendSMSSerializer(serializers.Serializer):
    group = serializers.PrimaryKeyRelatedField(queryset=Group.objects.all())
    body = serializers.CharField()
    eta = serializers.DateTimeField()
    recipients = serializers.PrimaryKeyRelatedField(queryset=Member.objects.all(), many=True)

class MemberViewSet(viewsets.ModelViewSet):
    queryset = Member.objects.all()
    serializer_class = MemberSerializer
    filter_backends = (DjangoFilterBackend, filters.SearchFilter)
    filter_fields = ('first_name', 'last_name', 'email', 'phone_mobi', 'position',
                     'language', 'contact_list', 'occupations', 'skills', 'memberships',
                     'birthday', 'gender', 'street', 'plz', 'city', 'status')
    search_fields = ('email', 'first_name', 'last_name', 'plz', 'city', 'street')

    @action(methods=['post'], detail=False)
    def send_sms(self, request, pk=None):
        serializer = SendSMSSerializer(request.data)
        if serializer.is_valid():
            group = serializer.cleaned_data.pop('group')
            content = serializer.cleaned_data['body']
            eta = serializer.cleaned_data['eta']
            recipients = serializer.cleaned_data['recipients']
            recipients = Member.objects.filter(pk__in==recipients)
            send_mail_mg_celery.apply_async(args=[content,
                django_serializers.serialize('json', recipients), group.pk], eta=eta)


    @action(methods=['post'], detail=False)
    def send_mailgun(self, request, pk=None):
        serializer = MailgunMailSerializer(request.data)
        if serializer.is_valid():
            group = serializer.cleaned_data.pop('group')
            recipients = serializer.cleaned_data['recipients']
            recipients = Member.objects.filter(pk__in==recipients)
            send_mail_mg_celery.apply_async(args=[serializer.cleaned_data,
                    django_serializers.serialize('json', recipients),
                    [], group.pk], eta=serializer.cleaned_data['eta'])


    def get_queryset(self):
        qs = super().get_queryset()
        if self.request.user.is_superuser:
            return qs
        groups = self.request.user.groups.all()
        return qs.filter(Q(position__in=groups) |
                         Q(imported_by=self.request.user)).filter(pending=False).distinct()

class SMSSerializer(serializers.HyperlinkedModelSerializer):
    member = serializers.PrimaryKeyRelatedField(read_only=True)
    twilio_account = serializers.PrimaryKeyRelatedField(read_only=True)
    class Meta:
        model = SMS
        fields = ('url', 'pk', 'content', 'member', 'incoming', 'time', 'twilio_account',
                'is_mass', 'location_string', 'twilio_sid', 'delivered')

class SMSViewSet(viewsets.ModelViewSet):
    queryset = SMS.objects.all()
    serializer_class = SMSSerializer
    filter_backends = (DjangoFilterBackend, filters.SearchFilter)
    filter_fields = ('twilio_account', 'content', 'member')
    search_fields = ('content',)

    def get_queryset(self):
        accounts = SMSSender.objects.filter(group__in=self.request.user.groups.all())
        return super().get_queryset().filter(twilio_account__in=accounts)

router = routers.DefaultRouter()
router.register(r'users', UserViewSet)
router.register(r'groups', GroupViewSet)
router.register(r'members', MemberViewSet)
router.register(r'memberships', MembershipViewSet)
router.register(r'skills', SkillViewSet)
router.register(r'occupations', OccupationViewSet)
router.register(r'contact-lists', ContactListViewSet)
router.register(r'letter-templates', LetterTemplateViewSet)
router.register(r'portalforms', PortalFormViewSet)
router.register(r'mail-templates', MailTemplateViewSet)
router.register(r'mailgun-messages', MailgunMessageViewSet)
router.register(r'mailgun-accounts', MailgunViewSet)
router.register(r'twilio-accounts', TwilioViewSet)
router.register(r'twilio-messages', SMSViewSet)



urlpatterns = [
    url(r'^', include(router.urls)),
]
