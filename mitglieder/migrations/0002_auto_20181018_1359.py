# Generated by Django 2.0.4 on 2018-10-18 13:59

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('mitglieder', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='member',
            name='last_active',
            field=models.DateTimeField(default=datetime.datetime(1970, 1, 1, 0, 0), verbose_name='Letzte Aktivität'),
        ),
    ]
