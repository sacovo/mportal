# Generated by Django 2.1.2 on 2018-10-20 16:50

from django.db import migrations
import tinymce.models


class Migration(migrations.Migration):

    dependencies = [
        ('mitglieder', '0008_auto_20181020_1837'),
    ]

    operations = [
        migrations.AlterField(
            model_name='portalform',
            name='confirmation_text',
            field=tinymce.models.HTMLField(default='<p>Hallo {member.first_name},</p>\n<p>\nDu hast bei {form.name} teilgenommen. Klicke auf disen Link, um deine Antwort zu bestätigen:\n</p>\n\n{confirmation_link}\n\n<p>\nVielen Dank und Freundliche Grüsse\n</p>\nXXX\n'),
        ),
    ]
