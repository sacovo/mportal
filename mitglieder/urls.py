# coding=utf-8
""" This file is part of Sektionsportal.
    Sektionsportal is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Sektionsportal is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Sektionsportal.  If not, see <http://www.gnu.org/licenses/>.
"""
from django.conf.urls import url
import mitglieder.views
from django.views.generic import RedirectView

urlpatterns = [
    url(r'^$', RedirectView.as_view(url="/admin/")),
    url(r'^send-mail/$', mitglieder.views.send_mail, name="send-mail"),
    url(r'^send-mail-mg/$', mitglieder.views.send_mail_mg, name="send-mail-mg"),
    url(r'^add-to-campaign/$', mitglieder.views.add_to_campaign, name="add-to-campaign"),
    url(r'^check-mail/$', mitglieder.views.check_mail, name="check-mail"),
    url(r'^add-to-campaign/$', mitglieder.views.add_to_campaign,
        {'remove': True}, name="remove-from-campaign"),
    url(r'^remove-from-campaign/$', mitglieder.views.add_to_campaign,
        {'remove': True}, name="remove-from-campaign"),
    url(r'^add-to-task/$', mitglieder.views.add_to_task, name="add-to-task"),
    url(r'^add-to-group/$', mitglieder.views.add_to_group, name="add-to-group"),
    url(r'^remove-from-group/$', mitglieder.views.add_to_group,
        name="remove-from-group", kwargs={'remove':True}),
    url(r'^chose-template/([0-9+])/$',
        mitglieder.views.chose_template, name="chose-template"),
    url(r'^render-letter/(?P<template>[0-9+])/$',
        mitglieder.views.render_letter, name="render-letter"),
    url(r'^export-excel/$', mitglieder.views.export_excel, name="export-excel"),
    url(r'^send-sms/', mitglieder.views.send_sms, name="send-sms"),
    url(r'^send-wa-tw/', mitglieder.views.send_wa_tw, name="send-wa-tw"),
    url(r'^send-wa-websms/', mitglieder.views.send_wa_websms, name="send-wa-websms"),
    url(r'import/', mitglieder.views._import, name="import"),
    url(r'twilio-callback/', mitglieder.views.twilio_callback, name="twilio-callback"),
    url(r'mailgun-webhook/', mitglieder.views.mailgun_webhook, name="mailgun_webhook"),
    url(r'twilio-chose/(?P<member>[0-9]+)/$',
        mitglieder.views.open_chat, name="chose-sender"),
    url(r'twilio-window/(?P<member>[0-9]+)/(?P<sender>[0-9]+)/$',
        mitglieder.views.chat_window, name="chat-window"),
    url(r'twilio-conversation/(?P<member>[0-9]+)/(?P<sender>[0-9]+)/$',
        mitglieder.views.conversation_view, name="conversation-view"),
    url(r'mailgun-report/(?P<pk>[0-9]+)/$',
        mitglieder.views.mailgun_report, name="mailgun-report"),
    url(r'forms/confirm/(?P<conf>[\w-]+)/$',
        mitglieder.views.confirm_view, name="form-confirmation"),
    url(r'forms/send-confirmation/$',
        mitglieder.views.send_form_link, name="send-form-link"),
    url(r'forms/(?P<slug>[\w-]+)/$',
        mitglieder.views.portalform_view, name="form-view"),
    url(r'results/(?P<slug>[\w-]+)/view/$',
        mitglieder.views.form_result_view, name="form-result-view"),
]
