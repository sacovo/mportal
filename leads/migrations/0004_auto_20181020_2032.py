# Generated by Django 2.1.2 on 2018-10-20 18:32

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('mitglieder', '0010_portalform_header'),
        ('leads', '0003_auto_20181019_2213'),
    ]

    operations = [
        migrations.AddField(
            model_name='subscriptionform',
            name='header',
            field=models.URLField(blank=True),
        ),
        migrations.AddField(
            model_name='subscriptionform',
            name='mailgun_user',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='mitglieder.MailGunUser'),
        ),
    ]
