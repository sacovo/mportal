from django.shortcuts import render, redirect, get_object_or_404

# Create your views here.
from leads.models import SubscriptionForm, SubscriptionAnswer, SubscriptionFieldAnswer
from leads.forms import LeadForm

from django.utils.translation import ugettext as _
from django.urls import reverse
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.contrib.sites.shortcuts import get_current_site
from mitglieder.models import Member
from django.contrib.auth.models import Group
from django.conf import settings

from ipware import get_client_ip



from mitglieder.views import random_string, get_hash
from django.core.mail import send_mail as django_send_mail

from mitglieder.tasks import send_sms as send_sms_celery,\
        send_mail as send_mail_celery, send_mail_mg as send_mail_mg_celery
from mitglieder.models import MailGunMessage

def send_notification(answer, request):
    m = _("Neue Anmeldung im Formular {answer.form.name}, von {answer.ip_address}")\
            .format(answer=answer)
    for field in answer.subscriptionfieldanswer_set.all():
        m += "\n{field.field.label}: {field.value}".format(field=field)
    django_send_mail(
            _("Neue Anmeldung [{answer.form.name}]").format(answer=answer),
            m,
            settings.DEFAULT_FROM_EMAIL,
            [answer.form.notify_mail]
    )

def send_confirmation_link(answer, request):
    form = answer.form
    s = random_string()
    link = 'https://' + get_current_site(request).domain + \
        reverse("subscription-confirmation", kwargs={'secret': str(s)})
    a_text = '<a href="{link}">{link}</a>'.format(link=link)
    message = form.confirm_mail
    data = {
        'sender': form.contact_mail,
        'name': form.contact_name,
        'subject': form.confirmation_subject.format(member=answer.member),
        'tag': 'form',
        'message': message,
        'template': form.mail_template.pk if form.mail_template else None,
        'img_url': form.header,
        'etype': 'trans'
    }
    if form.mailgun_user:
        mg_m = MailGunMessage.objects.create(
                group=form.mailgun_user.group,
                subject=form.confirmation_subject.format(member=answer.member),
                sender=form.contact_mail,
                content=message,
                open_points=0,
                click_points=0,
                remove_points=0
        )
        send_mail_mg_celery.delay(data, answer.member.pk, [], form.mailgun_user.group.pk, mg_m.pk,
                confirmation_link=link, a_text=a_text)
    else:
        send_mail_celery.delay(data, answer.member.pk, [], confirmation_link=link, a_text=a_text)
    answer.secret = get_hash(s)
    answer.save()

def confirm_view(request, secret):
    h = get_hash(secret)
    answer = get_object_or_404(SubscriptionAnswer, secret=h)
    answer.confirmed_mail = True
    answer.member.pending = False
    sform = answer.form
    groups = list(sform.target_groups.all())
    for field in sform.fields.filter(target='position'):
        f = SubscriptionFieldAnswer.objects.filter(
                field=field,
                answer=answer
        )[0]
        if field.input_type == 'forms.MultipleChoiceField':
            g = list(Group.objects.filter(name__in=f.value))
            groups += g
        if field.input_type == 'forms.ChoiceField':
            g = Group.objects.filter(name=f.value)[0]
            groups.append(g)
    answer.member.position.set(groups)
    answer.member.save()
    answer.secret = ''
    answer.save()
    if sform.notify_mail:
        send_notification(answer, request)
    return render(request, 'leads/confirmed.html', {'answer': answer})

@csrf_exempt
def form_view(request, pk):
    sform = SubscriptionForm.objects.get(pk=pk)
    form = LeadForm(form=sform)
    if request.POST:
        form = LeadForm(request.POST, form=sform)
        if form.is_valid():
            secret = random_string()
            client_ip, is_routable = get_client_ip(request)
            answer = SubscriptionAnswer.objects.create(
                    form = sform,
                    confirmed_mail = not sform.confirmation_mail,
                    secret = secret,
                    ip_address=client_ip if is_routable else None
            )
            member_dict = {
                    'contact_list': sform.target_list,
                    'pending': sform.confirmation_mail,
            }
            groups = list(sform.target_groups.all())
            for field in sform.fields.all():
                f = SubscriptionFieldAnswer.objects.create(
                        field=field,
                        value=form.cleaned_data[str(field.pk)],
                        answer=answer
                )
                f.save()
                if field.target == 'position':
                    if field.input_type == 'forms.MultipleChoiceField':
                        g = list(Group.objects.filter(name__in=form.cleaned_data[str(field.pk)]))
                        groups += g
                    if field.input_type == 'forms.ChoiceField':
                        g = Group.objects.filter(name=form.cleaned_data[str(field.pk)])[0]
                        groups.append(g)
                else:
                    member_dict[field.target] = f.value
            member = Member.objects.create(**member_dict)
            member.save()
            answer.member = member
            if sform.confirmation_mail:
                send_confirmation_link(answer, request)
            if sform.success_url:
                return render(request, 'leads/redirect.html', {'url': sform.success_url}, status=201)
    return render(request, sform.template, {'form':form, 'sform': sform})

def test_view(request):
    return render(request, 'form-test.html')

def get_status(request, pk):
    form = SubscriptionForm.objects.get(pk=pk)
    data = {
        'name': form.name,
        'amount': form.target_list.member_set.count()
    }
    return JsonResponse(data)

def script_view(request, pk):
    sform = SubscriptionForm.objects.get(pk=pk)
    return render(request, 'leads/form_script.js', {'pk':pk}, content_type='application/javascript')
