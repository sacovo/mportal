        $(document).ready(function() {
            function submit_form(event){
                event.preventDefault();
                fd = new FormData($('#{{pk}}-form')[0]);
                $.ajax({
                    url: '//{{ request.get_host }}{% url 'form-view' pk %}',
                    type: "POST",
                    data:fd,
                    processData: false,
                    contentType: false,
                    statusCode: {
                        200: function (response) {
                            $('#form-container-{{pk}}').html(response);
                            $('#{{pk}}-form').submit(submit_form);
                        },
                        201: function (response) {
                            window.location = response;
                        }
                    }
                });
                }
                    $.ajax({
                        url: '//{{ request.get_host }}{% url 'form-view' pk %}',
                        crossDomain: true
                    }).done(function(data){
                        $('#form-container-{{pk}}').html(data);
                        $('#{{pk}}-form').submit(submit_form);
                    });

            });
