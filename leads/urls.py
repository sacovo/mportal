# coding=utf-8
""" This file is part of Sektionsportal.
    Sektionsportal is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Sektionsportal is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Sektionsportal.  If not, see <http://www.gnu.org/licenses/>.
"""
from django.conf.urls import url
import leads.views
from django.views.generic import RedirectView

urlpatterns = [
    url(r'^(?P<pk>[0-9]+)/$', leads.views.form_view, name="form-view"),
    url(r'^(?P<pk>[0-9]+)/data.json$', leads.views.get_status, name="get-status"),
    url(r'^script/(?P<pk>[0-9]+)\.js$', leads.views.script_view, name="form-script"),
    url(r'forms/confirm/(?P<secret>[\w-]+)/$',
        leads.views.confirm_view, name="subscription-confirmation"),
]
