from django.contrib import admin

# Register your models here.
from suit.sortables import SortableTabularInline
from django.conf import settings

from django.urls import reverse_lazy

from leads.models import *
from reversion.admin import VersionAdmin

class FormFieldInline(SortableTabularInline):
    model = SubscriptionFormField
    extra = 3
    fields = ('target', 'input_type', 'label', 'css_classes',
            'help_text', 'required', 'choices', 'widget')
    ordering = ('ordering',)
    sortable = 'ordering'

class AnswerFieldInline(admin.TabularInline):
    model = SubscriptionFieldAnswer
    extra = 0
    fields = ("field", "value")
    readonly_fields = fields

@admin.register(SubscriptionForm)
class SubscriptionAdmin(VersionAdmin):
    list_display = ('name', 'target_list')
    readonly_fields = ('script_tag','container_tag', 'status_json')
    filter_horizontal = ('target_groups', 'users')
    inlines = [
            FormFieldInline
            ]

    def script_tag(self, instance):
        if instance.pk:
            url = 'https://' + settings.SITE_DOMAIN + reverse('form-script', args=(instance.pk,))
            return '<script src="{url}"></script>'.format(url=url)

    def container_tag(self, instance):
        if instance.pk:
            return '<div id="form-container-{pk}"></div>'.format(pk=instance.pk)

    def status_json(self, instance):
        if instance.pk:
            return 'https://' + settings.SITE_DOMAIN + reverse('get-status', args=(instance.pk,))

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(users=request.user)

    def get_form(self, request, obj=None, **kwargs):
        form = super().get_form(request, obj=obj, **kwargs)

        def init_func(self, data=None, files=None, **kwargs):
            super(form, self).__init__(data=data, files=files, **kwargs)
            self.fields['target_list'].queryset = request.user.contactlist_set.all()
            self.fields['target_groups'].queryset = request.user.groups.all()
        form.__init__ = init_func
        return form

@admin.register(SubscriptionAnswer)
class SubscriptionAnsewrAdmin(VersionAdmin):
    inlines = [AnswerFieldInline]
    list_display = ('__str__', 'member', 'time_submitted', 'confirmed_mail', 'ip_address')

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(form__users=request.user)

@admin.register(MailTrigger)
class MailTriggerAdmin(VersionAdmin):
    list_display = ('group', 'mailgun_user', 'subject', 'from_mail')
    filter_horizontal = ['users']

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(form__users=request.user)

