from django import forms

from django.utils.translation import ugettext as _
from captcha.fields import ReCaptchaField

import requests
import json

def mail_valid(email, pub_key):
    url = "https://api.mailgun.net/v3/address/private/validate"
    if pub_key.startswith('pub'):
        url = "https://api.mailgun.net/v3/address/validate"
    r = requests.get(url, auth=('api', pub_key), params={
        'address': email,
        'mailbox_verification': True
    })
    r = json.loads(r.text)
    if r['is_valid'] and not r['is_disposable_address']:
        return 0
    if r['is_valid']:
        return 1
    return 2


class LeadForm(forms.Form):
    """Docstring"""

    def __init__(self, *args, **kwargs):
        form = kwargs.pop('form')
        super(LeadForm, self).__init__(*args, **kwargs)
        self.subscriptionform = form
        for form_field in form.fields.all():
            self.fields[str(form_field.pk)] = form_field.get_instance()
        self.fields['captcha'] = ReCaptchaField(form.public_key, form.private_key, label="")

    def clean(self):
        cleaned_data = super().clean()
        sform = self.subscriptionform
        contact_list = sform.target_list
        if sform.fields.filter(target='email').exists():
            email = cleaned_data[str(sform.fields.filter(target='email')[0].pk)]
            if contact_list.member_set.filter(email=email).exists():
                raise forms.ValidationError(
                    _("Mail Addresse bereits vorhanden. ")
                )
            if sform.mailgun_user:
                if sform.mailgun_user.pub_key and sform.mail_checking != 3:
                    code = mail_valid(email, sform.mailgun_user.pub_key)
                    if sform.mail_checking == 1 and code != 0:
                        raise forms.ValidationError(
                        _("Ungültige E-Mail-Addresse") if code == 2 else _("Keine Wegwerfaddressen erlaubt")
                        )
                    elif sform.mail_checking == 2 and code != 1:
                        raise forms.ValidationError(
                        _("Ungültige E-Mail-Addresse")
                        )


        return cleaned_data
