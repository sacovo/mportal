from django.db import models
from django.utils.translation import ugettext as _
from django.contrib.auth.models import Group, User
from tinymce.models import HTMLField
from django.urls import reverse
from django.conf import settings

from mitglieder.models import ContactList, Member, MailGunUser, MailTemplate


# Create your models here.

field_choices = (
    ('om_number', _("OM Nummer")),
    ('first_name', _("Vorname")),
    ('last_name', _("Nachname")),
    ('street', _("Strasse")),
    ('plz', _("PLZ")),
    ('city', _("Ort")),
    ('language', _("Sprache")),
    ('phone_priv', _("Telefon privat")),
    ('phone_busi', _("Telefon Geschäftlich")),
    ('phone_mobi', _("Mobil")),
    ('country', _("Land")),
    ('email', _("E-Mail")),
    ('gender', _('Geschlecht')),
    ('letter_opening', _("Briefanrede")),
    ('birthday', _("Geburtstag")),
    ('position', _("Gruppen")),
    ('birthday_day', _("Geburtstag Tag")),
    ('birthday_month', _("Geburtstag Monat")),
    ('birthday_year', _("Geburtstag Year")),
    ('allow_contact', _("Kontakt erlauben")),
)

FORM_INPUT_TYPES = (
    ('forms.DateField', _('Datum')),
    ('forms.TimeField', _('Zeit')),
    ('forms.CharField', _('Text')),
    ('forms.ChoiceField', _('Auswahl')),
    ('forms.MultipleChoiceField', _('Mehrfachauswahl')),
    ('forms.BooleanField', _('Wahrheitswert')),
    ('forms.DecimalField', _('Zahl')),
    ('forms.EmailField', _('E-Mail')),
    ('forms.URLField', _('URL')),
    ('forms.FileField', _('Datei')),
)

FORM_WIDGETS = (
    ('forms.PasswordInput', _("Passwort-Widget")),
    ('forms.Textarea', _("Langer Text")),
    ('forms.SelectInput', _("Select-Widget")),
    ('forms.Select', _("Select-Widget")),
    ('forms.SelectMultiple', _("Mehrfach-Select")),
    ('forms.RadioSelect', _("Select-Radio-Widget")),
    ('forms.CheckboxSelectMultiple', _("Multipleselect Checkbox")),
)

class SubscriptionForm(models.Model):
    name = models.CharField(max_length=100, verbose_name=_("Name"))
    get_prefill = models.BooleanField()
    target_list = models.ForeignKey(ContactList,\
            on_delete=models.CASCADE, verbose_name=_("Ziel-Liste"))
    target_groups = models.ManyToManyField(Group, verbose_name=_("Ziel-Gruppen"))
    success_url = models.URLField(blank=True, verbose_name=_("Erfolgs-Weiterleitung"))
    success_message = HTMLField(verbose_name=_("Erfolgsnachricht"),
            help_text=_("Angezeigt, bei erfolgreichem Absenden des Formulars"))
    submit_text = models.CharField(max_length=100, default="Absenden", verbose_name=_("Senden-Text"), help_text=_("Text auf dem Absenden Knopf"))
    confirmation_mail = models.BooleanField(verbose_name=_("Bestätigungsmail"), help_text=_("Müssen die Anmeldungen per Mail bestätigt werden?"))
    confirmation_subject = models.CharField(max_length=100, blank=True, verbose_name=_("Betreff"), 
            help_text=_("Betreff des Bestägigungsmails, {contact.first_name} funktioniert."))
    confirmation_success = HTMLField(blank=True, verbose_name=_("Bestätigungstext"), help_text=_("Text der nach der Bestätigung des Mails angezeigt wird."))
    contact_name = models.CharField(blank=True, max_length=200, verbose_name=_("Sender*in-Name"))
    contact_mail = models.EmailField(blank=True, verbose_name=_("Bestätigungsmail-Addresse"))
    mailgun_user = models.ForeignKey('mitglieder.MailGunUser', models.CASCADE, blank=True, null=True)
    mail_template = models.ForeignKey('mitglieder.MailTemplate', models.CASCADE, blank=True, null=True)
    header = models.URLField(blank=True)
    confirm_mail = HTMLField(blank=True, verbose_name=_("Bestätigungsmail-Inhalt"), help_text=_("Inhalt des Besätigungsmails.Django Templating funktioniert!<br> {{a_text}} => &lt;a href=''...&gt; Link zur Bestätigung. Oder {{confirmation_link}} => https://... Die URL zur Bestätigung."))
    notify_mail = models.EmailField(blank=True, verbose_name=_("Benachrichtung an"))
    users = models.ManyToManyField(User, verbose_name="Benutzer*innen")
    template = models.CharField(default="leads/form_view.html", max_length=200,
            choices=settings.FORM_TEMPLATE_CHOICES, verbose_name=_("Formular-Vorlage"))

    public_key = models.CharField(max_length=200, blank=True,
            help_text=_("Publik-Key für die Seite(n) auf der das Formular angezeigt wird. <a href='https://www.google.com/recaptcha/admin'>https://www.google.com/recaptcha/admin</a>"))
    private_key = models.CharField(max_length=200, blank=True,
            help_text=_("Publik-Key für die Seite(n) auf der das Formular angezeigt wird. <a href='https://www.google.com/recaptcha/admin'>https://www.google.com/recaptcha/admin</a>"))
    MAIL_CHECK_CHOICES = ((1, _("Gültig und Wegwerf")), (2, _("Gültig")), (3, _("Aus")))
    mail_checking = models.IntegerField(default=1, choices=MAIL_CHECK_CHOICES)

    class Meta:
        verbose_name = _("Anmeldeformular")
        verbose_name_plural = _("Anmeldeformulare")

    def answer_count(self):
        return self.fields.count()

    def __str__(self):
        return self.name

def get_field_class(input_str):
    from django import forms
    return eval(input_str)


class SubscriptionFormField(models.Model):
    target = models.CharField(max_length=100, choices=field_choices, blank=True, verbose_name=_("Ziel-Feld"))
    input_type = models.CharField(max_length=50, choices=FORM_INPUT_TYPES, verbose_name=_("Eingabetyp"))
    label = models.CharField(max_length=500, verbose_name=_("Label"))
    css_classes = models.CharField(max_length=100, blank=True)
    help_text = models.TextField(blank=True, verbose_name=_("Hilfstext"))
    required = models.BooleanField(verbose_name=_("Benötigt"))
    form = models.ForeignKey(SubscriptionForm, on_delete=models.CASCADE, related_name="fields")
    ordering = models.IntegerField(default=1)
    choices = models.TextField(
        _("Auswahl"), blank=True, help_text=_("Eine Option pro Zeile"))
    widget = models.CharField(_("Widget"), blank=True,
                              choices=FORM_WIDGETS, max_length=100)
    def get_instance(self):
        kwargs = dict()
        kwargs['label'] = self.label
        kwargs['required'] = self.required
        if self.choices:
            kwargs['choices'] = ((s.strip(), s.strip())
                                 for s in self.choices.split('\n'))
        if self.widget:
            kwargs['widget'] = get_widget(self.widget)
        return get_field_class(self.input_type)(**kwargs)

    def __str__(self):
        return self.label

    class Meta:
        ordering = ['ordering']


class SubscriptionAnswer(models.Model):
    form = models.ForeignKey(SubscriptionForm, on_delete=models.CASCADE)
    member = models.ForeignKey(Member, on_delete=models.CASCADE, blank=True, null=True)
    confirmed_mail = models.BooleanField()
    time_submitted = models.DateTimeField(auto_now_add=True)
    secret = models.CharField(max_length=130)
    ip_address = models.GenericIPAddressField(blank=True, null=True)


    def __str__(self):
        return self.form.name

    class Meta:
        verbose_name = _("Anmeldung")
        verbose_name_plural = _("Anmeldungen")

class SubscriptionFieldAnswer(models.Model):
    field = models.ForeignKey(SubscriptionFormField, on_delete=models.CASCADE)
    value = models.TextField()
    answer = models.ForeignKey(SubscriptionAnswer, on_delete=models.CASCADE)

    class Meta:
        ordering = ['field']

    def _str__(self):
        return self.field.label

class MailTrigger(models.Model):
    group = models.ForeignKey(Group, models.CASCADE, verbose_name="Gruppe")
    mailgun_user = models.ForeignKey(MailGunUser, models.CASCADE, verbose_name="Mailgun-Zugang")

    from_name = models.CharField(max_length=200, verbose_name="Von (name)")
    from_mail = models.EmailField(verbose_name="Von (e-mail)")

    subject = models.CharField(max_length=200, verbose_name="Betreff")
    content = HTMLField(verbose_name="Inhalt")
    header = models.URLField(verbose_name="Header-URL")

    template = models.ForeignKey(MailTemplate, models.CASCADE)

    remove_points = models.IntegerField(default=0, verbose_name=_("Punktabzug"))
    open_points = models.IntegerField(default=0, verbose_name=_("Öffnungspunkte"))
    click_points = models.IntegerField(default=0, verbose_name=_("Klickpunkte"))

    users = models.ManyToManyField(User, verbose_name="Zugriff")

    class Meta:
        verbose_name = _("Mail Trigger")
        verbose_name_plural = _("Mail Trigger")

    def __str__(self):
        return self.subject
