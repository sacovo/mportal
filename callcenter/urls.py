# coding=utf-8
""" This file is part of Sektionsportal.
    Sektionsportal is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Sektionsportal is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Sektionsportal.  If not, see <http://www.gnu.org/licenses/>.
"""
from django.conf.urls import url
import callcenter.views
from django.views.generic import RedirectView

urlpatterns = [
    url(r'^$', callcenter.views.select_application, name="select-view"),
    url(r'^callcenter/$', callcenter.views.callcenter_view, name="callcenter-view"),
    url(r'^call-hook/(?P<app>\w+)/$', callcenter.views.call, name="call-hook"),
    url(r'^ajax/status/$', callcenter.views.status_callback, name="status-callback"),
    url(r'^ajax/operators/$', callcenter.views.operators, name="operators"),
    url(r'^ajax/contacts/$', callcenter.views.get_contacts, name="contacts"),
    url(r'^ajax/contact/$', callcenter.views.get_contact_detail, name="contact-detail"),
    url(r'^ajax/contact/block/$', callcenter.views.block_contact, name="block-contact"),
    url(r'^ajax/contact/submit/$', callcenter.views.submit_call, name="submit-call"),
    url(r'^ajax/contact/add-note/$', callcenter.views.add_note, name="add-note"),
]
