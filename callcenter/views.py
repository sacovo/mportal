import datetime
from datetime import timedelta

from django.utils.dateparse import parse_datetime
from django.utils.timezone import is_aware, make_aware

from django.utils import timezone
from django.http import HttpResponse
from django.http import JsonResponse
from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.decorators import login_required
from django.core.exceptions import PermissionDenied
from django.core import serializers

from django.template.loader import render_to_string, get_template

from twilio.twiml.voice_response import VoiceResponse, Dial, Say
from twilio.twiml.messaging_response import MessagingResponse
from twilio.rest import Client
from twilio.jwt.client import ClientCapabilityToken



from callcenter.models import PhoneNumber, Application, Operator, Call, CampaignForm,\
        CampaignContact, Answer
from callcenter.forms import CallForm

from mitglieder.models import Member, Note


def check_user(user, app):
    # Check if the user is allowed in this application
    if user.is_superuser:
        return True
    for group in user.groups.all():
        if group in app.groups.all():
            return True
    return False

@login_required
def add_note(request):

    member = Member.objects.get(pk=request.POST['member'])
    subject = request.POST['subject']
    content = request.POST['content']
    user = request.user

    Note.objects.create(
            member=member,
            subject=subject,
            content=content,
            user=user
        )

    return JsonResponse({'status':'ok'})



@login_required
def select_application(request):
    if request.user.is_superuser:
        applications = Application.objects.all()
    else:
        applications = Application.objects.filter(groups__in=request.user.groups.all())

    return render(request, 'callcenter/select_application.html', context = {
        'applications': applications
    })


@login_required
def callcenter_view(request):
    if 'app' not in request.POST:
        return redirect('select-view')
    app = Application.objects.get(pk=request.POST['app'])

    if not check_user(request.user, app):
        raise PermissionDenied
    operator, created = Operator.objects.get_or_create(
            name=request.user.username,
            application=app,
            defaults={'online': True}
    )

    groups = request.user.groups.all()
    if request.user.is_superuser:
        campaigns = CampaignForm.objects.all()
    else:
        campaigns = CampaignForm.objects.filter(groups__in=groups)

    operator.online = True
    operator.save()

    capability = ClientCapabilityToken(app.account_sid, app.auth_token)
    capability.allow_client_outgoing(app.application_sid)
    capability.allow_client_incoming(operator.name)
    token = capability.to_jwt()

    return render(request, 'callcenter/callcenter_view.html', context={
        'application': app,
        'operator': operator,
        'token': token.decode('utf-8'),
        'campaigns': campaigns,
    })


@csrf_exempt
def change_status(request, app):
    operator = Operator.objects.get(name=request.user.username, apppliaction=app)

    if request.POST['operator-status'] == 'online':
        operator.online = True
    else:
        operator.online = False
    operator.save()

    return JsonResponse({
        'online': operator.online,
    })

def operators(request):
    application = Application.objects.get(pk=request.POST['Application'])
    operators = application.operator_set.filter(online=True).values('name', 'online')

    now = parse_datetime(request.POST['StartTime'])

    missed_calls = Call.objects.filter(missed=True,start__gt=now,application=application).values(
            'start', 'caller', 'called')

    r =  JsonResponse({
        'operators': list(operators),
        'missed': list(missed_calls),
        })
    return r


def status_callback(request):
    op = Operator.objects.get(pk=request.POST['Operator'])
    if request.POST['Event'] == 'Accepted incoming':
        op.online = False
    if request.POST['Event'] == 'Call ended':
        op.online = True
    if request.POST['Event'] == 'Error':
        op.online = False
    if request.POST['Event'] == 'Ready':
        op.online = True
    if request.POST['Event'] == 'Connected':
        op.online = False
    if request.POST['Event'] == 'Offline':
        op.online = False
    if request.POST['Event'] == 'Online':
        op.online = True

    op.save()

    return JsonResponse({
        })


@login_required
def get_contacts(request):
    # Return contacts for a campaign
    campaign = CampaignForm.objects.get(pk=request.POST['campaign'])

    if not check_user(request.user, campaign):
        raise PermissionDenied

    now = timezone.localtime()

    status = request.POST['status']
    query = campaign.contacts.filter(
            campaigncontact__blocked=False
        )
    total = campaign.contacts.all().count()
    closed = campaign.contacts.filter(campaigncontact__closed=True).count()

    available = campaign.contacts.filter(campaigncontact__blocked=False,
            campaigncontact__locked_until__lt=now,
            campaigncontact__closed=False).count()

    status_str = "{closed} von {total} erreicht, noch {available} offen".format(
            total=total, closed=closed, available=available)
    if status == 'open':
        query = query.filter(
                campaigncontact__locked_until__lt=now,
                campaigncontact__closed=False
                )
    if status == 'blocked':
        query = query.filter(
                campaigncontact__locked_until__gt=now)
    if status == 'closed':
        query = query.filter(
                campaigncontact__closed=True)
    contacts = query.values(
                    'first_name', 'last_name', 'campaigncontact__pk',
                    'campaigncontact__closed', 'campaigncontact__blocked',
                    'phone_mobi', 'phone_busi', 'phone_priv')
    application = Application.objects.get(pk=request.POST['Application'])
    operators = application.operator_set.filter(online=True).values('name', 'online')

    now = parse_datetime(request.POST['StartTime'])

    missed_calls = Call.objects.filter(missed=True,start__gt=now,application=application).values(
            'start', 'caller', 'called')

    return JsonResponse({
        'data': list(contacts),
        'operators': list(operators),
        'missed': list(missed_calls),
        'status': status_str,
    })

def get_aware_datetime(date_str):
    ret = parse_datetime(date_str)
    if not is_aware(ret):
        ret = make_aware(ret)
    return ret

@login_required
def submit_call(request):
    contact = CampaignContact.objects.get(pk=request.POST['contact'])
    campaign = contact.campaign

    if not check_user(request.user, campaign):
        raise PermissionDenied

    for q in campaign.question_set.all():
        if q.name in request.POST:
            a = Answer.objects.create(
                    question=q,
                    result=contact,
                    answer=request.POST[q.name],
                    user=request.user
                    )
            a.save()
    contact.closed = True
    contact.blocked = False
    contact.user = None
    contact.closed_at = timezone.localtime()
    contact.save()

    return JsonResponse({'status': 'ok'})


@login_required
def block_contact(request):
    contact = CampaignContact.objects.get(pk=request.POST['contact'])
    campaign = contact.campaign

    if not check_user(request.user, campaign):
        raise PermissionDenied
    blocked_until = timezone.localtime()
    if request.POST['event'] == 'skip':
        blocked_until += timedelta(minutes=20)
    if request.POST['event'] == 'not-avail':
        blocked_until += timedelta(hours=4)
    if request.POST['event'] == 'until':
        blocked_until = get_aware_datetime(request.POST['blocked-until'])

    contact.contact.campaigncontact_set.update(locked_until=blocked_until, blocked=False, user=None)

    return JsonResponse({'status': 'ok'})


@login_required
def get_contact_detail(request):
    contact = CampaignContact.objects.get(pk=request.POST['contact'])
    campaign = contact.campaign

    if not check_user(request.user, campaign):
        raise PermissionDenied

    request.user.campaigncontact_set.update(blocked=False)
    request.user.campaigncontact_set.update(user=None)

    contact = CampaignContact.objects.get(pk=request.POST['contact'])
    campaign = contact.campaign

    if contact.blocked:
        return JsonResponse({'status': 'blocked'})

    if 'close' in request.POST:
        return JsonResponse({
            'name': '',
            'form': '',
            'contact_info': '',
            'status': 'ok',
            })

    contact.contact.campaigncontact_set.update(blocked = True, user=request.user)
    # We render the form
    initial = dict()
    if contact.closed:
        for a in contact.answer_set.all():
            initial[a.question.name] = a.answer
    form = CallForm(campaign=campaign, contact=contact, initial=initial)
    contact_info = render_to_string('callcenter/contact_info.html',
            {'contact': contact}, request=request)
    render_form = render_to_string('callcenter/call_form.html',
            {'form': form, 'contact': contact}, request=request)


    return JsonResponse({
        'name': contact.contact.first_name,
        'form': render_form,
        'contact_info': contact_info,
        'status': 'ok',
        })

@csrf_exempt
def recording_callback(request):
    pass


@csrf_exempt
def call(request, app):
    app = Application.objects.get(pk=app)

    response = VoiceResponse()

    to = request.POST['To']
    number = to
    incoming = True
    operator = None

    if 'FromNumber' not in request.POST:
        # We recieved a phone call, try to connect to operater
        query = app.operator_set.filter(online=True)
        dial = Dial()
        if query.exists():
            for op in query:
                # Connect to all online clients of the application
                dial.client(op.name)
        else:
            # No operator active, try to forward to saved number(s)
            print("Forwarding")
            numbers = PhoneNumber.objects.get(number=to).forward_to.strip().split('\n')
            if numbers[0] != '':
                # Call all saved numbers, the first one to pick it up wins
                for number in numbers:
                    dial.number(number)
            else:
                #Nobody available, sorry :(
                response.say(
                        'Momentan ist leider niemand verfügbar, versuchen Sie es später wieder.',
                        language="de", voice="man"
                        )
                Call.objects.create(
                        incoming=True,
                        missed=True,
                        start=timezone.localtime(),
                        end=timezone.localtime(),
                        sid=request.POST['CallSid'],
                        caller = request.POST['From'],
                        called=request.POST['To'],
                        application=app
                    )
                return HttpResponse(str(response))
        incoming = True
        number = request.POST['From']

        response.append(dial)
    else:
        # A user called a number
        dial = Dial(caller_id=request.POST['FromNumber'])
        dial.number(request.POST['To'])
        print(request.POST)
        operator = Operator.objects.filter(
                name=request.POST['From'].rpartition(':')[2],
                application=app
                )[0]
        response.append(dial)

    call = Call.objects.create(
            incoming=incoming,
            start=timezone.localtime(),
            end=timezone.localtime(),
            sid=request.POST['CallSid'],
            operator=operator,
            caller=request.POST['From'],
            called=request.POST['To'],
            application=app
            )
    call.save()

    return HttpResponse(str(response))
