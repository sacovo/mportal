from django.contrib import admin
from reversion.admin import VersionAdmin
from callcenter.models import Application, PhoneNumber, Call, CampaignForm, CampaignContact\
        , Question, Answer, Operator
from django.http import HttpResponseRedirect, HttpResponse
from django.utils import timezone

import csv

from suit.sortables import SortableTabularInline
# Register your models here.

class NumberInline(admin.TabularInline):
    model = PhoneNumber
    extra = 1

@admin.register(Application)
class ApplicationAdmin(VersionAdmin):
    list_display = ('name', 'application_sid')
    inlines = [NumberInline]
    filter_horizontal = ['groups']

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        if request.user.is_superuser:
            return qs
        groups = request.user.groups.all()
        return qs.filter(groups__in=groups)


@admin.register(Call)
class CallAdmin(VersionAdmin):
    list_display = ('sid','operator', 'contact', 'incoming', 'start', 'end')
    def get_queryset(self, request):
        qs = super().get_queryset(request)
        if request.user.is_superuser:
            return qs
        groups = request.user.groups.all()
        return qs.filter(application__groups__in=groups)

class QuestionInline(SortableTabularInline):
    model = Question
    extra = 1
    sortable = 'ordering'


@admin.register(CampaignForm)
class CampaignFormAdmin(VersionAdmin):
    list_display = ('name', )
    inlines = [QuestionInline]
    filter_horizontal = ['groups']

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        if request.user.is_superuser:
            return qs
        groups = request.user.groups.all()
        return qs.filter(groups__in=groups)

class AnswerInline(admin.TabularInline):
    model = Answer
    extra = 0

    def has_add_permission(self, request):
        return False

    def has_change_permission(self, request, obj=None):
        # returning false causes table to not show up in admin page :-(
        # I guess we have to allow changing for now
        if not obj:
            return True
        if obj in request.user.groups.all():
            return True
        return False

@admin.register(CampaignContact)
class CampaignContactAdmin(VersionAdmin):
    list_display = ('contact', 'campaign', 'locked_until', 'closed', 'blocked', 'user')
    list_filter = ("campaign", "closed")
    search_fields = ("contact__first_name", "contact__last_name",
            "contact__phone_priv", "contact__phone_busi", "contact__phone_mobi",
            "contact__street", "contact__city")
    inlines = [AnswerInline]
    suit_list_filter_horizontal = ('campaign', )
    actions = ['export']

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        if request.user.is_superuser:
            return qs
        groups = request.user.groups.all()
        return qs.filter(campaign__groups__in=groups)

    def export(self, request, queryset):
        response = HttpResponse(content_type='text/csv')
        writer = csv.writer(response, delimiter=";", quotechar='"', quoting=csv.QUOTE_MINIMAL)
        campaigns = queryset.values('campaign').distinct()
        questions = Question.objects.filter(form__in=campaigns)
        contact_fields = ['first_name', 'last_name', 'email', 'phone_mobi', 'phone_busi',
                'phone_priv', 'street', 'city', 'plz', 'pk']
        campaign_fields = ['campaign', 'locked_until', 'closed_at']
        header = contact_fields + campaign_fields + [q.name for q in questions]

        writer.writerow(header)

        for m in queryset:
            c = m.contact
            line = [getattr(c, f) for f in contact_fields] + [getattr(m, f) for f in campaign_fields]
            line += [m.get_answer(q) for q in questions]
            writer.writerow(line)
        dt = timezone.localtime().strftime('%y%m%d%H%M')
        campaigns = CampaignForm.objects.filter(pk__in=campaigns)
        fname = 'export_' + '_'.join([c.name for c in campaigns]) + '_' + dt  + '.csv'

        response['Content-Disposition'] = 'attachement; filename="' + fname + '"'

        return response
    export.short_description = "Exportieren"


@admin.register(Operator)
class OperatorAdmin(admin.ModelAdmin):
    list_display = ('name', 'application', 'online')
    list_filter = ['application']

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        if request.user.is_superuser:
            return qs
        groups = request.user.groups.all()
        return qs.filter(application__groups__in=groups)
