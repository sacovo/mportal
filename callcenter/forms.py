
import os
from datetime import datetime

from django import forms
from django.utils.translation import ugettext as _

class CallForm(forms.Form):
    def __init__(self, *args, **kwargs):
        campaign = kwargs.pop('campaign')
        contact = kwargs.pop('contact', None)
        super(CallForm, self).__init__(*args, **kwargs)
        self.campaign = campaign
        for form_field in campaign.question_set.all():
            self.fields[form_field.name] = form_field.get_instance()

