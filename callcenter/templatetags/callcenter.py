from django.template import engines, Library
from django.utils.safestring import mark_safe

register = Library()
@register.filter()
def international(value, arg):
    local_phone = value.replace(' ', '').replace('-', '').replace('(', '').replace(')', '')
    if local_phone == '':
        return ''
    if local_phone[0] == '+':
        return local_phone
    if local_phone[0] == '0':
        local_phone = local_phone[1:]
    return arg + local_phone
