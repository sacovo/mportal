from django.db import models
from django.contrib.auth.models import Group, User
from mitglieder.models import Member, get_field_class
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone
# Create your models here.

class Application(models.Model):
    """Stores token and everything for twilio."""
    groups = models.ManyToManyField(Group) # Access Management
    application_sid = models.CharField(max_length=200, verbose_name=_("Anwendungs SID"))
    account_sid = models.CharField(max_length=200, verbose_name=_("Account SID"))
    auth_token = models.CharField(max_length=200, verbose_name=_("Auth Token"))
    name = models.CharField(max_length=100, verbose_name=_("Name"))

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _("Anwendung")
        verbose_name_plural = _("Anwendungen")

class Operator(models.Model):
    application = models.ForeignKey(Application, models.CASCADE, verbose_name=_("Anwendung"))
    online = models.BooleanField(default=False)
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name + '@' + self.application.name

class PhoneNumber(models.Model):
    """Used for handling call forwarding"""
    application = models.ForeignKey(Application, on_delete=models.CASCADE, verbose_name=_("Anwendung"))
    number = models.CharField(max_length=20, blank=True, verbose_name=_("Nummer"))
    forward_to = models.TextField(help_text=_("Mehrere Nummern mit neuer Zeile trennen, max. 10"), blank=True, verbose_name=_("Weiterleitungen"))

    def __str__(self):
        return self.number

    class Meta:
        verbose_name = _("Telefonnummer")
        verbose_name_plural = _("Telefonnummern")

class Call(models.Model):
    """Store calls"""
    incoming = models.BooleanField(verbose_name=_("Eingehend"))
    start = models.DateTimeField(_("Start"))
    end = models.DateTimeField(_("Ende"))
    contact = models.ForeignKey(Member, on_delete=models.CASCADE, blank=True, null=True, verbose_name=_("Kontakt"))
    recording = models.FileField(upload_to='recordings/%Y/%m/%d/', blank=True, verbose_name=_("Aufzeichnung"))
    sid = models.CharField(max_length=100)
    operator = models.ForeignKey(Operator, on_delete=models.CASCADE, blank=True, null=True)
    missed = models.BooleanField(default=False)
    caller = models.CharField(max_length=120, blank=True)
    called = models.CharField(max_length=120, blank=True)
    application = models.ForeignKey(Application, on_delete=models.CASCADE, blank=True, null=True)

    def __str__(self):
        return self.caller + '<->' + self.called


    class Meta:
        verbose_name = "Anruf"
        verbose_name_plural = "Anrufe"
        ordering = ('-start', )

class CampaignForm(models.Model):
    """ Store questions and the contacts to be called. """
    name = models.CharField(max_length=100)
    contacts = models.ManyToManyField(Member, through='CampaignContact', verbose_name=_("Kontakte"))
    groups = models.ManyToManyField(Group)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _("Kampagnen Formular")
        verbose_name_plural = _("Kampagnen Formulare")
        ordering = ['name']

class CampaignContact(models.Model):
    contact = models.ForeignKey(Member, on_delete=models.CASCADE, verbose_name=_("Kontakt"))
    campaign = models.ForeignKey(CampaignForm, on_delete=models.CASCADE, verbose_name=_("Kampagne"))
    calls = models.ManyToManyField(Call, related_name="+", blank=True, verbose_name=_("Anrufe"))
    closed = models.BooleanField(_("Abgeschlossen"))
    locked_until = models.DateTimeField(default=timezone.localtime, verbose_name=_("Blockiert bis"))
    blocked = models.BooleanField(default=False, verbose_name=_("Blockiert"))
    user = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, null=True)
    closed_at = models.DateTimeField(default=timezone.localtime)

    @property
    def is_locked(self):
        return self.locked_until > timezone.localtime()

    def __str__(self):
        return self.contact.full_name + "@" + self.campaign.name

    def get_answer(self, question):
        if self.answer_set.filter(question=question).exists():
            return self.answer_set.filter(question=question)[0].answer
        return ''

    class Meta:
        verbose_name = _("Kampagnenkontakt")
        verbose_name_plural = _("Kampagnenkontakte")

FORM_INPUT_TYPES = (
    ('forms.DateField', _('Datum')),
    ('forms.TimeField', _('Zeit')),
    ('forms.CharField', _('Text')),
    ('forms.ChoiceField', _('Auswahl')),
    ('forms.MultipleChoiceField', _('Mehrfachauswahl')),
    ('forms.BooleanField', _('Wahrheitswert')),
    ('forms.DecimalField', _('Zahl')),
    ('forms.EmailField', _('E-Mail')),
    ('forms.URLField', _('URL')),
    ('forms.FileField', _('Datei')),
)

WIDGETS = (
    ('forms.PasswordInput', _("Passwort-Widget")),
    ('forms.Textarea', _("Langer Text")),
    ('forms.SelectInput', _("Select-Widget")),
    ('forms.Select', _("Select-Widget")),
    ('forms.SelectMultiple', _("Mehrfach-Select")),
    ('forms.RadioSelect', _("Select-Radio-Widget")),
    ('forms.CheckboxSelectMultiple', _("Multipleselect Checkbox")),
)

def get_widget(widget_str):
    from django import forms
    return eval(widget_str)


class Question(models.Model):
    name = models.CharField(max_length=30, verbose_name=_("Name"))
    form = models.ForeignKey(CampaignForm, on_delete=models.CASCADE, verbose_name=_("Kampagne"))
    input_type = models.CharField(max_length=50, choices=FORM_INPUT_TYPES, verbose_name=_("Typ"))
    label = models.TextField(verbose_name=_("Frage"))
    ordering = models.IntegerField(default=1, verbose_name=_("Sortierung"))
    required = models.BooleanField(_("Benötigt"), default=True)
    initial = models.CharField(_("Default"), blank=True, max_length=300)
    choices = models.TextField(
        _("Auswahl"), blank=True, help_text=_("Eine Option pro Zeile"))
    widget = models.CharField(_("Widget"), blank=True,
                              choices=WIDGETS, max_length=100)

    def src_string(self):
        return self.input_type + '(' + self.argument_string + ')'

    def get_instance(self):
        kwargs = dict()
        kwargs['label'] = self.label
        kwargs['required'] = self.required
        if self.initial:
            kwargs['initial'] = self.initial
        if self.choices:
            kwargs['choices'] = ((s.strip(), s.strip())
                                 for s in self.choices.split('\n'))
        if self.widget:
            kwargs['widget'] = get_widget(self.widget)
        return get_field_class(self.input_type)(**kwargs)

    def __str__(self):
        return self.label

    class Meta:
        ordering = ['form','ordering', 'label']
        verbose_name = _("Frage")
        verbose_name_plural = _("Fragen")

    def __str__(self):
        return self.label

class Answer(models.Model):
    question = models.ForeignKey(Question, on_delete=models.CASCADE, verbose_name=_("Frage"))
    result = models.ForeignKey(CampaignContact, models.CASCADE, verbose_name=_("Kontakt"))
    answer = models.TextField(verbose_name=_("Antwort"))
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True, blank=True)

    def __str__(self):
        return self.question.name + " Answer"

    class Meta:
        verbose_name = _("Antwort")
        verbose_name_plural = _("Antworten")
